# Open source hardware - fields of application in technology science

A lecture was given by Robert Mies (TUB)  in the Winter Semester 2022-23 on 21 November 2022, 14-16 Uhr c.t. in the frame of the 2nd Lecture Series "Open Science and Research Quality" under the Objective 3 “Advancing Research Quality and Value” of the Berlin University Alliance (see URL: <https://www.berlin-university-alliance.de/en/commitments/research-quality/news/2022/20221011_rv-forschungsqualitaet-open-science.html>).

The main topics of the lecture were:

- background on the concept of open source hardware
- empirical findings and best practices
- technology assessment
- standardisation of practices
- fields of application
- case examples
